<!--==========================
  Footer
============================--> 
  <footer id="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="copyright">
              &copy; Copyright <strong>Imperial Theme</strong>. All Rights Reserved
            </div>
            <div class="credits">
              <!-- 
                All the links in the footer should remain intact. 
                You can delete the links only if you purchased the pro version.
                Licensing information: https://bootstrapmade.com/license/
                Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Imperial
              -->
              Bootstrap Themes by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
          </div>
        </div>
      </div>
  </footer><!-- #footer -->
  
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    
  <!-- Required JavaScript Libraries -->
  <script src="<?php echo get_bloginfo('template_directory'); ?>/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo get_bloginfo('template_directory'); ?>/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo get_bloginfo('template_directory'); ?>/lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo get_bloginfo('template_directory'); ?>/lib/superfish/hoverIntent.js"></script>
  <script src="<?php echo get_bloginfo('template_directory'); ?>/lib/superfish/superfish.min.js"></script>
  <script src="<?php echo get_bloginfo('template_directory'); ?>/lib/morphext/morphext.min.js"></script>
  <script src="<?php echo get_bloginfo('template_directory'); ?>/lib/wow/wow.min.js"></script>
  <script src="<?php echo get_bloginfo('template_directory'); ?>/lib/stickyjs/sticky.js"></script>
  <script src="<?php echo get_bloginfo('template_directory'); ?>/lib/easing/easing.js"></script>
  
  <!-- Template Specisifc Custom Javascript File -->
  <script src="<?php echo get_bloginfo('template_directory'); ?>/js/custom.js"></script>  
    
</body>
</html>