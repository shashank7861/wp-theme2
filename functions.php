<?php
	// add a favicon to your 
	function blog_favicon() {
		echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('wpurl').'/favicon.ico" />';
	}
	add_action('wp_head', 'blog_favicon');
	
	// add a favicon for your admin
	function admin_favicon() {
		echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('stylesheet_directory').'/images/favicon.png" />';
	}
	add_action('admin_head', 'admin_favicon');

	// customize admin footer text
	function custom_admin_footer() {
		echo '<a href="http://example.com/">Website Design by Awesome Example</a>';
	} 
	add_filter('admin_footer_text', 'custom_admin_footer');

	// spam & delete links for all versions of wordpress
	function delete_comment_link($id) {
		if (current_user_can('edit_post')) {
			echo '| <a href="'.get_bloginfo('wpurl').'/wp-admin/comment.php?action=cdc&c='.$id.'">del</a> ';
			echo '| <a href="'.get_bloginfo('wpurl').'/wp-admin/comment.php?action=cdc&dt=spam&c='.$id.'">spam</a>';
		}
	}

	// Add scripts and stylesheets
	function startwordpress_scripts() {
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/lib/bootstrap/css/bootstrap.min.css', array(), '3.3.6' );
		wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/lib/font-awesome/css/font-awesome.min.css' );
		wp_enqueue_style( 'style', get_template_directory_uri() . 'css/style.css' );
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/lib/bootstrap/js/bootstrap.min.js', array( 'jquery' ), '3.3.6', true );
	}

	// Add Google Fonts
	function startwordpress_google_fonts() {
					wp_register_style('OpenSans', 'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800');
					wp_enqueue_style( 'OpenSans');
			}

	add_action('wp_print_styles', 'startwordpress_google_fonts');

	// WordPress Titles
	add_theme_support( 'title-tag' );

	// Custom settings
	function custom_settings_add_menu() {
	  add_menu_page( 'Custom Settings', 'Custom Settings', 'manage_options', 'custom-settings', 'custom_settings_page', null, 99 );
	}
	add_action( 'admin_menu', 'custom_settings_add_menu' );

	// Create Custom Global Settings
	function custom_settings_page() { ?>
	  <div class="wrap">
	    <h1>Custom Settings</h1>
	    <form method="post" action="options.php">
	       <?php
	           settings_fields( 'section' );
	           do_settings_sections( 'theme-options' );      
	           submit_button(); 
	       ?>          
	    </form>
	  </div>
	<?php }

	function setting_twitter() { ?>
	  <input type="text" name="twitter" id="twitter" value="<?php echo get_option( 'twitter' ); ?>" />
	<?php }

	function setting_github() { ?>
	  <input type="text" name="github" id="github" value="<?php echo get_option('github'); ?>" />
	<?php }

	function setting_facebook() { ?>
	  <input type="text" name="facebook" id="facebook" value="<?php echo get_option('facebook'); ?>" />
	<?php }

	function register_my_menus() {
	  register_nav_menus(
	    array(
	      'header-menu' => __( 'Header Menu' ),
	      'extra-menu' => __( 'Extra Menu' )
	    )
	  );
	}
	add_action( 'init', 'register_my_menus' );

	function custom_settings_page_setup() {
	  add_settings_section( 'section', 'All Settings', null, 'theme-options' );
	  add_settings_field( 'twitter', 'Twitter URL', 'setting_twitter', 'theme-options', 'section' );
	  add_settings_field( 'github', 'GitHub URL', 'setting_github', 'theme-options', 'section' );
	  add_settings_field( 'facebook', 'Facebook URL', 'setting_facebook', 'theme-options', 'section' );
  

	  register_setting('section', 'twitter');
	  register_setting('section', 'github');
	  register_setting('section', 'facebook');
	}
	add_action( 'admin_init', 'custom_settings_page_setup' );

	// Support Featured Images
	add_theme_support( 'post-thumbnails' );

	// Custom Post Type
	function create_my_custom_post() {
		register_post_type( 'my-custom-post',
				array(
				'labels' => array(
						'name' => __( 'My Custom Post' ),
						'singular_name' => __( 'My Custom Post' ),
				),
				'public' => true,
				'has_archive' => true,
				'supports' => array(
						'title',
						'editor',
						'thumbnail',
					  'custom-fields'
				)
		));
	}
	add_action( 'init', 'create_my_custom_post' );

	//logo
	add_theme_support( 'custom-logo', array(
		'height'      => 100,
		'width'       => 400,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );

	// custom comments callback function
	function custom_comments_callback($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment; ?>

		<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
			<div class="comment-wrap">
				<?php echo get_avatar(get_comment_author_email(), $size = '50', $default = bloginfo('stylesheet_directory').'/images/gravatar.png'); ?>

				<div class="comment-intro">
	            			<?php printf(__('%s'), get_comment_author_link()); ?> &ndash; <a class="comment-permalink" href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)); ?>"><?php comment_date('F j, Y'); ?> @ <?php comment_time(); ?></a><?php edit_comment_link('Edit', ' &ndash; ', ''); ?>
				</div>
				<?php if ($comment->comment_approved == '0') : ?>

				<p class="comment-moderation"><?php _e('Your comment is awaiting moderation.'); ?></p>
				<?php endif; ?>

				<div class="comment-text"><?php comment_text(); ?></div>

				<div class="reply" id="comment-reply-<?php comment_ID(); ?>">
					<?php comment_reply_link(array_merge($args, array('reply_text'=>'Reply', 'login_text'=>'Log in to Reply', 'add_below'=>'comment-reply', 'depth'=>$depth, 'max_depth'=>$args['max_depth']))); ?> 

				</div>
			</div>

	<?php } // WP adds the closing </li>

	/**
	 * Register our sidebars and widgetized areas.
	 *
	 */
	function arphabet_widgets_init() {

		register_sidebar( array(
			'name'          => 'Home right sidebar',
			'id'            => 'home_right_1',
			'before_widget' => '<div>',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="rounded">',
			'after_title'   => '</h2>',
		) );

	}
	add_action( 'widgets_init', 'arphabet_widgets_init' );
	// Function to return user count
	function wpb_user_count() {
		$usercount = count_users();
		$result = $usercount['total_users'];
		return $result;
	}


	?>
