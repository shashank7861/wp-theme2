<section id="about">
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <h3 class="section-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
          <div class="section-title-divider"></div>
          <p class="section-description"><p class="blog-post-meta"><?php the_date(); ?> by <a href="#"><?php the_author(); ?></a></p></p>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p class="about-text">
            <?php the_excerpt(); ?>
          </p>
          <div class="col-lg-12 well">
          	<a href="<?php comments_link(); ?>">
				<?php
					printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'textdomain' ), number_format_i18n(get_comments_number() ) ); ?>
			</a>
          </div>
        </div>
      </div>
    </div>
  </section>