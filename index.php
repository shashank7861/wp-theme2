<?php get_header(); ?>
<div class="container-fluid">
  <div class="row" id='post'>
    <div class="col-lg-10">
      <?php 
        if ( have_posts() ) : while ( have_posts() ) : the_post();
          get_template_part( 'content-single', get_post_format() );

          if ( comments_open() || get_comments_number() ) :
            comments_template();
          endif;

        endwhile; endif; 
      ?>
      <nav>
        <ul class="pager">
          <li><?php next_posts_link( 'Previous' ); ?></li>
          <li><?php previous_posts_link( 'Next' ); ?></li>
        </ul>
      </nav>
    </div>
    <div class="col-lg-2">
          <?php get_sidebar(); ?>
    </div>
  </div>
</div>
<!--==========================
  Subscrbe Section
============================-->  
  <section id="subscribe">
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-8">
          <h3 class="subscribe-title">Subscribe For Updates</h3>
          <p class="subscribe-text">Join our 1000+ subscribers and get access to the latest tools, freebies, product announcements and much more!</p>
        </div>
        <div class="col-md-4 subscribe-btn-container">
          <a class="subscribe-btn" href="#">Subscribe Now</a>
        </div>
      </div>
    </div>
  </section>
    
<!--==========================
  Testimonials Section
============================--> 
  <section id="testimonials">
    <div class="container wow fadeInUp">
      
    </div>
  </section>

<?php get_footer();?>  
