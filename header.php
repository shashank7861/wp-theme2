<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Imperial Boootstrap Template</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet"> 
  <!-- Bootstrap CSS File -->
  <link href="<?php echo get_bloginfo('template_directory'); ?>/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Libraries CSS Files -->
  <link href="<?php echo get_bloginfo('template_directory'); ?>/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo get_bloginfo('template_directory'); ?>/lib/animate-css/animate.min.css" rel="stylesheet">
  <!-- Main Stylesheet File -->
  <link href="<?php echo get_bloginfo('template_directory'); ?>/css/style.css" rel="stylesheet">
  <?php wp_head(); ?>
</head>
<body>
  <div id="preloader"></div>
  
<!--==========================
  Hero Section
============================-->
  <section id="hero">
    <div class="hero-container">
      <div class="wow fadeIn">
        <div class="hero-logo">
          <!-- <img class="" src="" alt="<?php echo get_bloginfo( 'name' ); ?>"> -->
          <?php 
            if ( function_exists( 'the_custom_logo' ) ) {
                the_custom_logo();
            }
          ?>
        </div>
        
        <h1><?php echo get_bloginfo( 'name' ); ?></h1>
        <h2><span class="rotating"><?php echo get_bloginfo( 'description' ); ?></span></h2>
        <div class="actions">
          <a href="#post" class="btn-get-started">Get Strated</a>
          <a href="/th/wp-admin/" class="btn-services">Log In</a>
        </div>
      </div>
    </div>
  </section>
  
<!--==========================
  Header Section
============================-->
  <header id="header">
    <div class="container">
    
      <div id="logo" class="pull-left">
        <!-- Uncomment below if you prefer to use a text image -->
        <h1><a href="<?php echo get_bloginfo( 'wpurl' );?>"><?php echo get_bloginfo( 'name' ); ?></a></h1>
      </div>
      <nav id="nav-menu-container">
        <?php $args=array(
           'theme_location'=>'header-menu',
           'menu_class'=>'nav-menu'
        );?>
        <?php wp_nav_menu($args); ?>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->